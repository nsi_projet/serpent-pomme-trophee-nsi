import pygame
import time
import random

pygame.init()

#pour les couleurs
white = (255, 255, 255)
yellow = (255, 255, 102)
black = (0, 0, 0)
red = (213, 50, 80)
green = (193, 25, 100)
blue = (200, 200, 200)

#pour l'affichage graphique sur pygame
dis_width = 600
dis_height = 400

dis = pygame.display.set_mode((dis_width, dis_height))
pygame.display.set_caption('Jeu serpent')

#le temps
clock = pygame.time.Clock()

#le serpent/vers
snake_block = 10

#couleur d'écriture
font_style = pygame.font.SysFont("bahnschrift", 25)
score_font = pygame.font.SysFont("comicsansms", 35)

#le score
def Your_score(score):
    value = score_font.render("Your Score: " + str(score), True, yellow)
    dis.blit(value, [0, 0])

#initialiser le vers (position et taille)
def our_snake(snake_block, snake_list):
    for x in snake_list:
        pygame.draw.rect(dis, black, [x[0], x[1], snake_block, snake_block])

#couleur et taille du message affiché
def message(msg, color):
    mesg = font_style.render(msg, True, color)
    dis.blit(mesg, [dis_width / 6, dis_height / 3])

#fonction pour le jeu
def gameLoop():
    game_over = False
    game_close = False

    #définir les positions
    x1 = dis_width / 2
    y1 = dis_height / 2

    #les changements
    x1_change = 0
    y1_change = 0

    #taille (longueur) du vers
    snake_List = []
    Length_of_snake = 1
    snake_speed = 13

    #positions de la pomme sur l'axe horizontal et vertical
    foodx = round(random.randrange(0, dis_width - snake_block) / 10.0) * 10.0
    foody = round(random.randrange(0, dis_height - snake_block) / 10.0) * 10.0
    start_time = pygame.time.get_ticks()    #définis le temps de départ

    while not game_over:

        while game_close == True:
            #affichage du jeu
            dis.fill(blue)  #couleur
            message("You Lost! Press C-Play Again or Q-Quit", red)  #message
            Your_score(Length_of_snake - 1) #score
            pygame.display.update() #mets à jour

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:    #si touche pressée
                    if event.key == pygame.K_q:     #si la touche est Q
                        game_over = True            #quitte le jeu
                        game_close = False
                    if event.key == pygame.K_c:     #si touche C pressée
                        gameLoop()                  #recommence le jeu
        for event in pygame.event.get():
            if event.type == pygame.QUIT:       #quitte le jeu
                game_over = True
            if event.type == pygame.KEYDOWN:    #si une touche est pressée
                if event.key == pygame.K_LEFT:  #si touche flèche gauche
                    x1_change = -snake_block    #le faire aller à gauche par la position
                    y1_change = 0               #reste sur le même axe horizontal
                elif event.key == pygame.K_RIGHT:   #si touche flèche droite pressée
                    x1_change = snake_block         #le faire aller à droite par la position
                    y1_change = 0                   #reste sur le même axe horizontal
                elif event.key == pygame.K_UP:      #si touche flèche haut pressée
                    y1_change = -snake_block        #le fait monter par la position
                    x1_change = 0                   #reste sur le même axe vertical
                elif event.key == pygame.K_DOWN:    #si touche bas pressée
                    y1_change = snake_block         #le faire descendre par la position
                    x1_change = 0                   #reste sur le même axe horizontal
        
        if x1 >= dis_width or x1 < 0 or y1 >= dis_height or y1 < 0: #si le vers sort de l'écran (cadre)
            game_close = True   #on a perdu
        x1 += x1_change     #pour applique le changement de position au vers à l'horizontal
        y1 += y1_change     #pareil mais  à la verticale
        dis.fill(blue)
        pygame.draw.rect(dis, green, [foodx, foody, snake_block, snake_block])  #dessine la pomme, l'affiche
        snake_Head = []         #liste pour la tête du vers
        snake_Head.append(x1)   #ajoute pour la position de la tête du vers
        snake_Head.append(y1)   #pareil
        snake_List.append(snake_Head)   #ajoute à l'autre liste (celle du corps)
        if len(snake_List) > Length_of_snake:
            del snake_List[0]

        for x in snake_List[:-1]:
            if x == snake_Head:
                game_close = True

        our_snake(snake_block, snake_List)
        Your_score(Length_of_snake - 1)     #score

        pygame.display.update() #mets à jour

        if x1 == foodx and y1 == foody: #si le vers mange la pomme
            foodx = round(random.randrange(0, dis_width - snake_block) / 10.0) * 10.0   #position de la nouvelle pomme sur vertical
            foody = round(random.randrange(0, dis_height - snake_block) / 10.0) * 10.0  #pareil sur l'horizontal
            Length_of_snake += 1    #taille du vers augmente
            snake_speed += 1

        if pygame.time.get_ticks() - start_time >= 5000:    #si plus de 5s sont passées depuis l'initialisation
            foodx = round(random.randrange(0, dis_width - snake_block) / 10.0) * 10.0   #nouvelle position de la pomme sur vertical
            foody = round(random.randrange(0, dis_height - snake_block) / 10.0) * 10.0  #pareil sur l'horizontal
            start_time = pygame.time.get_ticks()    #permets de mettre le compteur à zéro lors de la condition

        clock.tick(snake_speed) #vitesse du serpent

    pygame.quit()   #quitter pygame
    quit()          #quitter


gameLoop()  #recommencer le jeu
